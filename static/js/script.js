
// Survey Build===================================================================================
function share(elem){
    
    var parent = elem.parentNode;

    for(const child of parent.children) {
        if(child.id === "emailList")
            return;
      }
    
    var emailInput = document.createElement("input");
    emailInput.id = "emailList";
    emailInput.placeholder = "Enter a comma seperated email list.";
    emailInput.style = "width: 230px";
    parent.appendChild(emailInput);
}


//global to keep track of our questions
var questionID = 0;
function addQuestion()
{
    //get clone of html questionTemplate and all its children
    var cloneLi = document.getElementById("questionTemplate").cloneNode(true);
    cloneLi.style.visibility= "visible";
    //make a unique label
    var q = "question" + (questionID);
    cloneLi.id = q;  
    cloneLi.classList.add("questionsClass");  

    //look through the children of the clone to find answerList so it can be number accoridingly 
    var children = cloneLi.children;
    for (var i = 0; i < children.length; i++) {
        if(children[i].id === "answerList")
            children[i].id = ("answerList" + questionID);
    }    
    //increment questionID so it continues to be unique
    questionID++;

    //append the question clone to the questionlist
    document.getElementById("questionList").appendChild(cloneLi);

}

function addQuestionEdit()
{
    //get clone of html questionTemplate and all its children
    var cloneLi = document.getElementById("questionTemplate").cloneNode(true);
    console.log(cloneLi);
    cloneLi.style.visibility= "visible";
    //make a unique label
    var q = "question" + (questionID);
    cloneLi.id = q;  
    cloneLi.classList.add("questionsClass");  

    //look through the children of the clone to find answerList so it can be number accoridingly 
    var children = cloneLi.children;
    for (var i = 0; i < children.length; i++) {
        if(children[i].id === "answerList")
            children[i].id = ("answerList" + questionID);
    }    
    //increment questionID so it continues to be unique
    questionID++;

    //append the question clone to the questionlist
    document.getElementById("questionList").appendChild(cloneLi);

}

function subtractQuestion(elem)
{
    //find the question node
    var question = elem.parentNode.parentNode;
    //remove the question node from the questionList
    document.getElementById("questionList").removeChild(question);  

}


function addAnswer(elem) 
{ 
    
    //get children of the grandparent list
    var listChildren = elem.parentNode.parentNode.children;

    //value will be used to hold what is selected in the dropdown
    var value;
    for (let i = 0; i < listChildren.length; i++) {
        //find this nodes form element
        if(listChildren[i].id === "answerForm")
        {
            //look inside the form childrenList
            for(var o = 0; o < listChildren[i].length; o++)
            {
                //if we found the dropdown menu get the value selected
                if(listChildren[i].children[o].id === "drpdn")
                {
                    var select = listChildren[i].children[o];
                    var selectedIndex = select.selectedIndex
                    value = select.options[selectedIndex].value;                    
                }

            }
        }    
    }

    var element;
    //loop through to find answerList and then make it a unique string
    for (let i = 0; i < listChildren.length; i++) {
        if(listChildren[i].id.startsWith("answerList"))
        {
            element = document.getElementById(listChildren[i].id);
        }    
    }

    var createdElement;
    var input;
    if(value === "Multiple Choice")
    {
        //create the elements 
        createdElement = document.createElement("li");
        input = document.createElement("input");
        
        //set the to answer
        input.placeholder = input.id = "answer";

        //put the input in the list and put the element in the list
        createdElement.appendChild(input);
    }
    else if(value === "Likert5")
    {       
        
        createdElement = listChildren[2];
        createdElement.class = "likert";

        //input  
        var dots = 5;
        for(var p = 0; p < dots; p++)
        {
            var li = document.createElement("li");
            li.class = "likert";
            
            let textInput = document.createElement("input");
            
            li.appendChild(textInput);

            textInput.id = "textInput";
            if(p == 0 || p == 2 || p == dots-1)
            {  
                textInput.placeholder = "Scale: Like/Neutral/Dislike"
                li.appendChild(textInput); 

            }
            else
                li.appendChild(textInput);  
           
            li.style.listStyleType = "none";
            createdElement.appendChild(li);
            
        }
    }
    else if(value === "Short Answer")
    {
        let textArea = document.createElement("textarea");
        createdElement = document.createElement("li");
        
        textArea.placeholder = "answer";
        createdElement.style.listStyleType = "none";
        createdElement.appendChild(textArea);
        //set the to answer

    }
    else if(value === "TF")
    {
        createdElement = document.createElement("ol");

        let l1 = document.createElement("li");
        let l2 = document.createElement("li");

        let input1 = document.createElement("input");
        let input2 = document.createElement("input");

        let label1 = document.createElement("label");
        let label2 = document.createElement("label");


        label1.for = "T";
        label2.for = "F";

        label1.innerHTML = "True";
        label2.textContent = "False";

  
        input1.type = "radio";
        input2.type = "radio";

        input1.name = "TF";
        input2.name = "TF";

        input1.value = "T"
        input2.value = "F"

        //put the input in the list and put the element in the list
        createdElement.style.listStyleType = "none";


        l1.appendChild(label1);
        l1.appendChild(input1);

        l2.appendChild(label2);
        l2.appendChild(input2);

        createdElement.appendChild(l1);
        createdElement.appendChild(l2);
    }

    if(value != "Likert5")
        element.appendChild(createdElement);

}

function subtractAnswer(elem) 
{ 
    //find the grandparents children
    var children = elem.parentNode.parentNode.children;
    var element;

    //find the answer list element
    for (var i = 0; i < children.length; i++) {
        if(children[i].id.startsWith("answerList"))
            element = document.getElementById(children[i].id);
    }

    //remove it
    element.removeChild(element.lastChild);    
    
}

function addAnswerEdit(elem) 
{ 
    
    

    var listChildren = elem.parentNode.parentNode.children;

    //value will be used to hold what is selected in the dropdown
    var value;
    for (let i = 0; i < listChildren.length; i++) {
        //find this nodes form element
        if(listChildren[i].id === "answerForm")
        {
            //look inside the form childrenList
            for(var o = 0; o < listChildren[i].length; o++)
            {
                //if we found the dropdown menu get the value selected
                if(listChildren[i].children[o].id === "drpdn")
                {

                    var select = listChildren[i].children[o];
                    var selectedIndex = select.selectedIndex

                    value = select.options[selectedIndex].value;  
                }
            }
        }    
    }

    var element = listChildren[2];

    var createdElement;
    var input;
    if(value === "Multiple Choice")
    {
        //create the elements 
        createdElement = document.createElement("li");
        input = document.createElement("input");
        
        //set the to answer
        input.placeholder = input.id = "answer";

        //put the input in the list and put the element in the list
        createdElement.appendChild(input);
    }
    else if(value === "Likert5")
    {       
        
        createdElement = listChildren[2].children[0].parentNode;
        createdElement.class = "likert";

        //input  
        var dots = 5;
        for(var p = 0; p < dots; p++)
        {
            var li = document.createElement("li");
            li.class = "likert";
            
            let textInput = document.createElement("input");
            
            li.appendChild(textInput);

            textInput.id = "textInput";
            if(p == 0 || p == 2 || p == dots-1)
            {  
                textInput.placeholder = "Scale: Like/Neutral/Dislike"
                li.appendChild(textInput); 

            }
            else
                li.appendChild(textInput);  
           
            li.style.listStyleType = "none";
            createdElement.appendChild(li);
            
        }
    }
    else if(value === "Short Answer")
    {
        let textArea = document.createElement("textarea");
        createdElement = document.createElement("li");
        
        textArea.placeholder = "answer";
        createdElement.style.listStyleType = "none";
        createdElement.appendChild(textArea);
        //set the to answer

    }
    else if(value === "TF")
    {
        createdElement = document.createElement("ol");

        let l1 = document.createElement("li");
        let l2 = document.createElement("li");

        let input1 = document.createElement("input");
        let input2 = document.createElement("input");

        let label1 = document.createElement("label");
        let label2 = document.createElement("label");


        label1.for = "T";
        label2.for = "F";

        label1.innerHTML = "True";
        label2.textContent = "False";

  
        input1.type = "radio";
        input2.type = "radio";

        input1.name = "TF";
        input2.name = "TF";

        input1.value = "T"
        input2.value = "F"

        //put the input in the list and put the element in the list
        createdElement.style.listStyleType = "none";


        l1.appendChild(label1);
        l1.appendChild(input1);

        l2.appendChild(label2);
        l2.appendChild(input2);

        createdElement.appendChild(l1);
        createdElement.appendChild(l2);
    }

    if(value != "Likert5")
    {
        element.appendChild(createdElement);

    }

}

function subtractAnswerEdit(elem) 
{ 
    //find the grandparents children
    var children = elem.parentNode.parentNode.children;

    //remove it
    if(children[2].children.length > 0)
        children[2].removeChild(children[2].lastChild);    
}

function sendSavedSurveyJSON(elem)
{
    let title = document.getElementById("surveyTitle").value;
    let desc = document.getElementById("surveyDesc").value;

    if(title == "")
        title = "emptyTitle";

    if(desc == "")
        desc = "emptyDesc";

    let questionsOL = document.querySelector("#questionList");
    let questionsLength = questionsOL.getElementsByClassName("questionsClass").length;

    let text = '{ "survey" : [' +
                            '{ "survey_title":"' + title + 
                            '", "survey_description":"' + desc +
                            '", "questions": [] }' +
                            '] }';

    const obj = JSON.parse(text);
          
    for(let i = 0; i < questionsLength; i++)
    {      
        //gets one question element at a time by getting the question list and finding the question at i and then getting the questiontitlebar at children[0]
        let questionTitleBar = document.getElementById("questionList").children[i].children[0];
                
        //get the title type and required values
        let qTitle = questionTitleBar.children[0].value;
        let qType = questionTitleBar.getElementsByTagName("select")[0].value;
        let qRequired = questionTitleBar.children[3].checked;


        //this can also be done by doing 
        // ... = JSON.parse({})
        // obj.survey[0].questions[i].questionTitle = qTitle; etc
        obj.survey[0].questions[i] = JSON.parse('{' + 
                                    ' "questionTitle": "' + qTitle + '" ,'       +
                                    ' "questionType": "'  + qType  + '" ,'       +
                                    ' "questionRequired": "' + qRequired + '" ,' +
                                    ' "questionAnswers": [""]'           +
                                    '}');

        //initialize the answers array
        var answers;
        //multiple choice answer list
        if(qType == "Multiple Choice")
        {
        //find the answerlist in the html
            let answerList = document.getElementById("questionList").children[i].children[2];
            //get all the elements that are li's
            let items = answerList.getElementsByTagName("li");
            //set the answers array to this size
            answers = new Array(items.length);

            //fill the answers array
            for(let j = 0; j < items.length; j++)
                answers[j] = items[j].children[0].value;

        } //Likert answer list same stuff as ^^
        else if(qType == "Likert5")
        {
            let answerList = document.getElementById("questionList").children[i].children[2].children[0];
            let items = answerList.getElementsByTagName("li");
         
            answers = new Array(items.length);

            for(let j = 0; j < items.length; j++)
                answers[j] = items[j].children[1].value;


        }//Short answer same stuff but a little different because its just one thing rather then an array
        else if(qType == "Short Answer")
        {
            let answerList = document.getElementById("questionList").children[i].children[2];
            let items = answerList.getElementsByTagName("li");

            //if there is no answer put "" in the JSON
            if(items.length > 0)
                answers = items[0].children[0].value;
            else
                answers = "";
        }
        else if(qType == "TF")
        {
            let answerList = document.getElementById("questionList").children[i].children[2];
    
            
            let items = answerList.getElementsByTagName("li");
            if(items.length > 0)
            {
                answers = new Array(2);
                answers[0] = items[0].children[1].value;
                answers[1] = items[1].children[1].value;
            }   
            

        }

        obj.survey[0].questions[i].questionAnswers = answers;
                
    }
        


        var inp = document.createElement("input");
        inp.name = "data";
        inp.value = JSON.stringify(obj);
        inp.type = "hidden";

        document.getElementById("optionsForm").appendChild(inp);

}



function sendSubmittedSurveyJSON(elem)
{
    sendSavedSurveyJSON(elem);
    //also send it to the submitted DB 
}
// End Survey build JS====================================================================================

// Start Survey Take JS ==================================================================================

function sendTakenSurveyJSON(elem)
{

    let responses = new Array(document.querySelector("#questionsOl").children.length);


    for(let i = 0; i < questionsOl.children.length; i++)
    {
        let answers = questionsOl.children[i];

        for(let j = 0; j < answers.getElementsByTagName("li")[0].getElementsByTagName("div")[0].getElementsByTagName("ol")[0].children.length; j++)
        {
            //look through the listItems and find what radio button is check... for MC LS and TF
            if(answers.getElementsByTagName("div")[0].id == "Multiple Choice" || answers.getElementsByTagName("div")[0].id == "Likert5" || answers.getElementsByTagName("div")[0].id == "TF")
            {
                if(answers.getElementsByTagName("div")[0].getElementsByTagName("ol")[0].children[j].getElementsByTagName("input")[0].checked)
                {
                    
                    //put it in the array
                    if(answers.getElementsByTagName("div")[0].getElementsByTagName("ol")[0].children[j].getElementsByTagName("input")[0].value == "")
                        responses[i] = j;
                    else
                        responses[i] = answers.getElementsByTagName("div")[0].getElementsByTagName("ol")[0].children[j].getElementsByTagName("input")[0].value;
                }
            }
            
            //find the textbox value
            if(answers.getElementsByTagName("div")[0].id == "Short Answer")
            {
                //put it in the array
                responses[i] = answers.getElementsByTagName("div")[0].getElementsByTagName("ol")[0].children[j].getElementsByTagName("textarea")[0].value;              
            }
        }

    }

   

    for(let i = 0; i < responses.length; i++)
    {
        if(responses[i] == null)
            responses[i] = "";
    }

    var inp = document.createElement("input");
    inp.name = "data";
    inp.value = JSON.stringify(responses);
    inp.type = "hidden";


    document.getElementById("optionsForm").appendChild(inp);


}

// End Survey Take JS ====================================================================================

// Sign up JS=============================================================================================
function pwMatch() 
{

    //test
    //get the input from text box
    var pw1 = document.getElementById("passwordC").value;
    var pw2 = document.getElementById("password2C").value;
    var match = false;
    var pwLength = false;

     
    //if the password match and they arent empty
    match = (pw1 === pw2 && pw1 != "");

    //if the password is longer then 5
    pwLength = (pw1.length > 5);

    if(pwLength)
        document.getElementById("pwMatchText").innerText = "Password Length: Good";
    else
        document.getElementById("pwMatchText").innerText = "Password Length must be more then 5";


    if(match)
        document.getElementById("pwLengthCheck").innerText = "Passwords Match";
    else
        document.getElementById("pwLengthCheck").innerText = "Passwords do not Match";

    console.log(match && pwLength);

    return (match && pwLength);

}

function sendSignInJSON()
{
    let match = pwMatch();
    //if they dont match
    if(!match)
        return false;

    let username =  document.getElementById("signUpuname").value;
    let email =  document.getElementById("uemail").value;
    let pw =  document.getElementById("pw1").value;

    let text = '{ "newUserInfo" : [' +
    '{ "username":" ' + username + '", "email":"' + email + '", "password":"' + pw + '" }' + ']}';
    
    const obj = JSON.parse(text);

    //send obj to database to be stored
    //NEEDS TO BE DONE

    return match;

}

function checkLoginValidity()
{
    let username =  document.getElementById("signInUname").value;
    let psw =  document.getElementById("psw").value;


    let text = '{ "signInInfo" : [' +
    '{ "username":" ' + username + '", "password":"' + psw + '" }' + ']}';
    

    const obj = JSON.parse(text);
    //send obj to database for comparison
    //if obj.username is found inside of the database and obj.password == what we have on file 
        //return true; 
    //else
        //return false;


    //RETURN TRUE FOR NOW SO THE PAGE WORKS LOL
    return true;

}

//End Survey Login in =======================================================================================

//Survey Page JS=============================================================================================


function getPosition(string, subString, index) {
    return string.split(subString, index).join(subString).length;
}

//elem is the input button
function approveSurvey(elem) 
{ 
    //get the div that the button is in
    var parent = elem.parentNode;


    var str = elem.parentNode.innerText;
    let title = str.substring(0,str.indexOf(','));

    let first = getPosition(str, "-", 1)+1;
    let secound = getPosition(str, "-", 2);
    let desc = str.substring(first,secound);

    let author = str.substring(getPosition(str, "Submitted by", 1)+13, str.length-1);

    //create and put together the button
    var createdInput = document.createElement("input");
    createdInput.type = "button";
    createdInput.value = "Remove";
    createdInput.onclick = function (){removeSurvey(this)};
    parent.appendChild(createdInput);

    //append the li to the approvedSurveys
    document.getElementById("approvedSurveys").appendChild(parent);

    //get the list
    var submittedSurveyList = document.getElementById("submittedSurveysID");

    //if there are 0 li put "no more surveys"
    if(submittedSurveyList.getElementsByTagName("li").length == 0)
    {
        //create a div
        const e = document.createElement('div'); 

        //create a textnode
        var textnode = document.createTextNode('No surveys need approval...'); 
        //put the text node into the div
        e.appendChild(textnode);
        
        //insert right after the UL
        submittedSurveyList.parentNode.insertBefore(textnode, submittedSurveyList.nextSibling);
    }   
    
    //remove the approve button 
    parent.removeChild(elem);

    let text = '{ "approvedSurvey" : [' +
    '{ "title":" ' + title + '", "desc":"' + desc + '", "author":"' + author + '", "approvedBy":"' + "whoEverIsLoggedIn" + '" }' + ']}';
    //const obj = JSON.parse(text);

}

function removeSurvey(elem) 
{
    //get the ul that this is in 
    var parent = elem.parentNode;


    var str = elem.parentNode.innerText;
    let title = str.substring(0,str.indexOf(','));

    let first = getPosition(str, "-", 1)+1;
    let secound = getPosition(str, "-", 2);
    let desc = str.substring(first,secound);


    let author = str.substring(getPosition(str, "Submitted by", 1)+13, getPosition(str, ",", 3));
    let approvedBy = str.substring(getPosition(str, "Approved by", 1)+12, str.length);

    let text = '{ "removedSurvey" : [' +
    '{ "title":" ' + title + '", "desc":"' + desc + '", "author":"' + author + '", "approvedBy":"' + approvedBy + '" }' + ']}';
    
    //const obj = JSON.parse(text);

    //remove the li that we clicked on
    parent.parentNode.removeChild(parent);
    
}

// End Survey Page JS