-- ndf17a_dbcreate.sql
-- IT325 Reeves
--

\c postgres
drop database if exists ndf17a_survey2;
CREATE DATABASE ndf17a_survey2 encoding 'UTF-8';
\c ndf17a_survey2;

--USERS
drop table if exists users;
create table users (
users_id int primary key,
users_email text,
password text
);

insert into users (users_id, users_email, password) values
(1, 'ndf17a@acu.edu',  'password'),
(2, 'Admin@admin.com', 'password'),
(3, 'notAdmin@no.com', 'password')
;


--ADMINS
drop table if exists admins;
create table admins (
admin_id int primary key,
admin_email text,
users_id int
);

insert into admins (admin_id, admin_email, users_id) values
(0, 'noadmin',  0), --ndf17a@acu.edu
(1, 'ndf17a@acu.edu',  1), --ndf17a@acu.edu
(2, 'Admin@admin.com', 2)  --Admin@admin.com
;


--SURVEYS
drop table if exists surveys;
CREATE TABLE surveys (
survey_id serial primary key,
survey_title text,
survey_description text,
created_date date, --YYYY-MM-DD
edit_date date, --YYYY-MM-DD
users_id int,
admin_id int
);

insert into surveys (survey_id, survey_title, survey_description, created_date, edit_date, users_id, admin_id) values
(0, 'Outside and Trees', 'For the granola people', '2001-11-22', '2001-11-20',                3, 1),
(1, 'Video Game Survey', 'A survey about video games for gamers', '2001-11-22', '2001-11-20', 3, 1),
(2, 'Favorite Type of Bears', 'These are some bears','2001-11-22', '2001-11-20',              2, 2),
(3, 'Icecream!', 'ICECREAM', '2001-11-22', '2001-11-20',                                      3, 0)
;



--questions
drop table if exists questions;
CREATE TABLE questions (
question_id int primary key,
text text,
question_type text,
question_required int,
survey_id int
);

insert into questions (question_id, text, question_type, question_required, survey_id) values
(0, 'How much do you like bears', 'Likert5',       0, 2),
(1, 'Best action game', 'Multiple Choice',         0, 1),
(2, 'Best Story game',  'Multiple Choice',         1, 1),
(3, 'How much do you love minecraft', 'Likert5',   0, 1),
(4, 'What is your favorite game', 'Short Answer',  1, 1),
(5, 'I like subnautica', 'TF',                     0, 1),
(6, 'Which bear is best', 'Multiple Choice',       0, 2),
(7, 'Which icecream is best', 'Multiple Choice',   0, 3),
(8, 'Its an aspen because of the way it is', 'TF', 0, 0)
;


--ANSWERS
drop table if exists answers;
CREATE TABLE answers (
answer_id int primary key,
answer text,
question_id int
);

insert into answers (answer_id, answer, question_id) values
(0, 'Black Ops' ,    1),
(1, 'Call Of Duty' , 1),
(2, 'BattleField' ,  1),
(3, 'Apex' ,         1),
(4, 'Fornite' ,      1),

(5, 'BOTW' ,  2),
(6, 'Zelda' , 2),
(7, 'LONK' ,  2),

(8, 'Like' ,     3),
(9, ' ' ,        3),
(10, 'Neutral' , 3),
(11, ' ' ,       3),
(12, 'Dislike' , 3),

(13, 'short answer' , 4),

(14, 'True',  5),
(15, 'False', 5),

(16, 'Black', 6),
(17, 'Red',   6),
(18, 'Panda', 6),
(19, 'Koala', 6),

(20, 'Vanilla',     7),
(21, 'Chocolate',   7),
(22, 'StrawBerry',  7),
(23, 'Rockie-Road', 7),

(24, 'Like' ,    0),
(25, ' ' ,       0),
(26, 'Neutral' , 0),
(27, ' ' ,       0),
(28, 'Dislike' , 0),

(29, 'True',  8),
(30, 'False', 8)
;

--CHOICES
drop table if exists choices;
CREATE TABLE choices (
choice_id int primary key,
choice text,
question_id int,
users_id int
);

insert into choices (choice_id, choice, question_id, users_id) values
(0, 'Fornite',      1, 2),
(1, 'LONK',         2, 2),
(2, 'Dislike',      3, 3),
(3, 'short answer', 4, 2),
(4, 'True',         5, 1),
(5, 'Koala',        6, 3),
(6, 'Vanilla',      7, 1),
(7, 'Neutral',      0, 1),
(8, 'True',         8, 2),
(9, 'False',        8,-1), 
(10, 'Like',        0,-1),
(11, 'Black',       6,-1),
(12, 'Vanilla',     7, 1),
(13, 'StrawBerry',  7, 1),
(14, 'Like',        0, 1),
(15, 'Panda',       6, 1),
(16, 'Black Ops',   1, 1),
(17, 'Zelda',       2, 1),
(18, 'Dislike',     3, 1),
(19, 'Dr. Reeves web app simulator', 4, 1),
(20, 'True',        5, 1)
;


        



CREATE ROLE boss WITH
	LOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'boss';

GRANT nicolas TO boss;
GRANT all on users to boss;
GRANT all on admins to boss;
GRANT all on surveys to boss;
GRANT all on questions to boss;
GRANT all on answers to boss;

