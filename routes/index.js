const express = require('express');


//requiring from routes folder
const speakersRoute = require('./speakers');
const adminSurveyViewRoute = require('./adminSurveyView');
const userSurveyViewRoute = require('./userSurveyView');
const surveyBuildRoute = require('./surveyBuild');
const surveyTakeRoute = require('./surveyTake');
const loginRoute = require('./login');
const mySurveyRoute = require('./mySurvey');
const surveyDataRoute = require('./surveyData');
const { request } = require('express');



const router = express.Router();

module.exports = params => {
  router.get('/', (request, response) => {
    response.render('layout', { pageTitle: 'Welcome', template: 'index' });
  });

  //when i go to this link, render the template (in pages) i.e. gets adminSurveyView.ejs
  // router.get('/adminSurveyView', (request, response) => {
  //   response.render('layout', { pageTitle: 'AdminSurveys', template: 'adminSurveyView' });
  // });


  router.use('/adminSurveyView', adminSurveyViewRoute(params));
  router.use('/userSurveyView',  userSurveyViewRoute(params));
  router.use('/login',           loginRoute(params));
  router.use('/mySurveys',       mySurveyRoute(params));
  router.use('/surveyTake',      surveyTakeRoute(params));
  router.use('/surveyBuild',     surveyBuildRoute(params));
  router.use('/surveyData',      surveyDataRoute(params));



  router.get('/about', (request, response) => {
    response.render('layout', { pageTitle: 'About', template: 'about', user: request.session.user });
  });

  router.get('/reevous', (request, response) => {
    response.render('layout', { pageTitle: 'Hello There', template: 'reevous', user: request.session.user });
  });

  router.get('/signOut', (request, response) => {
    response.render('layout', { pageTitle: 'SignOut', template: 'signOut', user: request.session.user });
  });

  router.get('/simpleSurveyView', (request, response) => {
    response.render('layout', { pageTitle: 'Simple Survey', template: 'simpleSurveyView', user: request.session.user });
  });



  //when i go to this link spit some data out
  //router.use('/api/adminSurveyView', adminSurveyViewRoute(params));
  router.use('/api/userSurveyView',  userSurveyViewRoute(params));
  router.use('/api/surveyTake',      surveyTakeRoute(params));
  router.use('/api/surveyBuild',     surveyBuildRoute(params));
  router.use('/api/speakers',            speakersRoute(params));

  return router;
};
