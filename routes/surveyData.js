const express = require('express');

const router = express.Router();

module.exports = params => {
  // gets surveyService from the bottom of index... NOT from services
  const { surveyService } = params;



  router.get('/', async (request, response) => {
    console.log("get /surveyData")
    const user = request.session.user ? request.session.user : '';
    const surveys = await surveyService.dbgetData();
    const questions = await surveyService.dbGetAllQuestions();
    const choices = await surveyService.dbGetAllChoices();
    const users = await surveyService.dbGetAllUsers();

    console.log(surveys);
    console.log(questions);
    console.log(choices);
    console.log(users);

    return response.render('layout', { pageTitle: 'My Surveys', template: 'surveyData', user: user, surveys: surveys, questions: questions, choices: choices, users: users });
  });

  return router;
};


