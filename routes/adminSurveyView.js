const express = require('express');

const router = express.Router();

module.exports = params => {
  const { surveyService } = params;

  router.get('/', async (request, response) => {
    try {
      const { password } = request.body;
      console.log('post /admin: ', password);

      const errors = request.session.feedback ? request.session.feedback.errors : false;
      const successMessage = request.session.feedback ? request.session.feedback.message : false;
      const user = request.session.user ? request.session.user : '';

      request.session.feedback = {};

      console.log('adminSurveyView.js \'/\' called ');
      return response.render('layout', {
        pageTitle: 'Admin View',
        template: 'adminSurveyView',
        user: user,
        successMessage,
      });
    } catch (err) {
      return next(err);
    }

    // return response.json(surveys);
  });

  router.get('/:id', (request, response) => {
    return response.redirect(`Detail page of ${request.params.shortname}`);
  });

  

  router.post('/', async (request, response) => {
    const { survey_id, survey_title, survey_description, created_date, edit_date, author_email, user, approve } = request.body;
    console.log(request.body);
    console.log(approve);

    //approve
    if(approve == "true")
      await surveyService.dbApproveSurvey(survey_id, user);
    
    //remove
    if(approve == "false")
      await surveyService.dbUnApproveSurvey(survey_id);


    
    return response.redirect('/adminSurveyView');
  });

  return router;
};
