const express = require('express');

const router = express.Router();

module.exports = params => {
  // gets surveyService from the bottom of index... NOT from services
  const { surveyService, loginService } = params;

  router.get('/', async (request, response) => {

    response.render('layout', { pageTitle: 'SurveyTake', template: 'surveyTake', user: request.session.user, id: request.params.id });
  });

  router.get('/:id', async (request, response) => {
    console.log("get /:id in surveyTake.js")
    let survey_id = request.params.id;
 
    const survey = await surveyService.dbgetSurveysOnId(survey_id);
    const questions = await surveyService.dbGetQuestions(survey_id);
    const answers = await surveyService.dbGetAnswers(survey_id);
/*
    console.log("////Survey//////");
    console.log(survey);
    console.log("////Questions//////");
    console.log(questions);
    console.log("////answers//////");
    console.log(answers);
*/
    response.render('layout', { pageTitle: 'SurveyTake', template: 'surveyTake', user: request.session.user, id: request.params.id, survey: survey, questions: questions, answers: answers });
  });

  router.post('/:id', async (request, response) => {
    const { data, user, survey_id} = request.body;
    console.log("!!!!!!!!!!!!!!!!!!!!!!!");
    console.log('post /surveyTake: ', data, user, survey_id);
    console.log("!!!!!!!!!!!!!!!!!!!!!!!\n\n");

    const survey = await surveyService.dbgetSurveysOnId(survey_id);
    const questions = await surveyService.dbGetQuestions(survey_id);
    const userinfo = await loginService.dbgetUserOnEmail(user);
    let user_id;

    if(userinfo.length == 0)
      user_id = -1;
    else 
      user_id = userinfo[0].users_id;


    const rs = await surveyService.dbInsertChoices(JSON.parse(data), questions, user_id);

    return response.redirect(`/surveyTake/${request.params.id}`);

  });

  return router;
};



