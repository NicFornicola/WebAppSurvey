const express = require('express');

const router = express.Router();

module.exports = params => {
  const { loginService } = params;

  router.get('/', async (request, response, next) => {
    try {
      const errors = request.session.feedback ? request.session.feedback.errors : false;
      const successMessage = request.session.feedback ? request.session.feedback.message : false;
      const user = request.session.user ? request.session.user : '';

      request.session.feedback = {};

      return response.render('layout', {
        pageTitle: 'Login',
        template: 'login',
        user: user,
        successMessage,
      });
    } catch (err) {
      return next(err);
    }
  });

  router.post('/', async (request, response) => {
    const { email, password }   = request.body;
    const { emailC, passwordC } = request.body;
    const { nameU } = request.body;
    console.log('post /login: ', email,  ' password: ', password);
    console.log('post /login: ', emailC, ' password: ', passwordC);
    console.log('post /login: ', nameU);


    if(emailC == null && email == null || emailC == null && email == null && nameU == "")
    {

      if(nameU == "")
      {
        request.session.user = "SimpletonUser";
      }
      else
      {
        request.session.user = nameU;
      }

      return response.redirect('/simpleSurveyView');
 
    }
    //if the create account field was empty
    else if(emailC == null)
    {
      try {
        console.log("Login");
        let rs = await loginService.dbgetUser(email, password);
  
        if (rs.length > 0) {
          console.log('Log in Creds Found');
          request.session.feedback = { message: 'Logged In' };
          request.session.user = email;
          //why cant i add 
          //request.session.nice = email;
          //why cant i use user in usersurveyView
  
  
          rs = await loginService.dbUserIsAdmin(email, password);
          response.locals.user = email;
  
          if(rs.length > 0)
          {
            console.log('Found admin Creds');
            return response.redirect('/adminSurveyView');
          }
          else{
            console.log('Non admin user');
            return response.redirect('/userSurveyView');
          }
  
        } 
        else {
          console.log('Login Failed');
  
          request.session.loggedin = false;
          request.session.user = '';
  
          request.session.feedback = {
            message: 'User / password mismatch',
          };
          return response.redirect('/login');
  
        }
      } catch (err) {
        console.log(`login.js error ${err}`);
      }
    }//if the login field is empty they must be trying to create an account
    else if(email == null)
    {
      console.log("Trying to create account");
      try {
        let rs = await loginService.dbgetUserOnEmail(emailC);
  
        if (rs.length > 0) {
          request.session.feedback = { message: 'There is already an account with this email' };
          request.session.user = email;

          return response.redirect('/login');
 
        } 
        else {
          request.session.feedback = { message: 'Account Created! Time to Login!' };
          request.session.user = email;
          loginService.dbInsertUser(emailC, passwordC);
          
          return response.redirect('/login');
  
        }
      } catch (err) {
        console.log(`login.js error ${err}`);
      }
    }
    
    
  });
  return router;
};
