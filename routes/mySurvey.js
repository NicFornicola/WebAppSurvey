const express = require('express');

const router = express.Router();

module.exports = params => {
  // gets surveyService from the bottom of index... NOT from services
  const { surveyService } = params;

  router.get('/', (request, response) => {
    
    response.render('layout', { pageTitle: 'My Surveys', template: 'mySurveys', user: request.session.user });
  });

  router.get('/:user', async (request, response) => {

    const user = request.params.user;
    const rs = await surveyService.dbgetMySurveys(user);

    try {
     
  
      console.log('All of ', user, ' surveys: ', rs);
      
    } 
    catch (err) {
      console.log(`mySurvey.js error ${err}`);
    }

    return response.render('layout', { pageTitle: 'My Surveys', template: 'mySurveys', user: user, surveys: rs});
  });

  router.post('/', async (request, response) => {
    const { user, survey_id } = request.body;

    await surveyService.dbDeleteSurvey(survey_id);
    const surveys = await surveyService.dbgetMySurveys(user);
    
    return response.render('layout', { pageTitle: 'My Surveys', template: 'mySurveys', user: user, surveys: surveys });
  });

  return router;
};


