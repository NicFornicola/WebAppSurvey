const express = require('express');

const router = express.Router();

module.exports = params => {
  const { surveyService } = params;

  router.get('/', async (request, response) => {
    try {
      const errors = request.session.feedback ? request.session.feedback.errors : false;
      const successMessage = request.session.feedback ? request.session.feedback.message : false;
      const user = request.session.user ? request.session.user : '';

      request.session.feedback = {};

      console.log('userSurveyView.js \'/\' called ');
      return response.render('layout', {
        pageTitle: 'User View',
        template: 'userSurveyView',
        user: user,
        successMessage,
      });
    } catch (err) {
      return next(err);
    }

    // return response.json(surveys);
  });

  router.get('/:shortname', (request, response) => {
    return response.send(`Detail page of ${request.params.shortname}`);
  });

  router.post('/', async (request, response) => {
    const { title, begindate } = request.body;
    console.log('post /survey: ', title, ' begindate: ', begindate);
    try {
      // exists?
      // const rs = await surveyService.dbCreateSurvey(title, begindate);
      const rs = await surveyService.createSurvey(title, begindate);
      console.log('survey.js post/ after createSurvey', rs);
      if (rs.length > 0) {
        console.log('survey.js query returned something');
        request.session.feedback = { message: 'Inserted' };
        request.session.title = '';
        request.session.begindate = '';
        // request.session.user = email;
      } else {
        request.session.title = '';
        request.session.begindate = '';

        request.session.feedback = {
          message: 'Survey not inserted',
        };
      }
    } catch (err) {
      console.log(`survey.js error ${err}`);
    }
    return response.redirect('/survey');
  });

  return router;
};
