const express = require('express');

const router = express.Router();

module.exports = params => {
  // gets surveyService from the bottom of index... NOT from services
  const { surveyService } = params;

  router.get('/', (request, response) => {
    response.render('layout', { pageTitle: 'SurveyBuild', template: 'surveyBuild', user: request.session.user });
  });

  router.get('/:id', async (request, response) => {
    console.log("get /surveyBuild/:id");

    const survey    = await surveyService.dbgetSurveysOnId(request.params.id);
    const questions = await surveyService.dbGetQuestions(request.params.id);
    const answers   = await surveyService.dbGetAnswers(request.params.id);

    console.log(survey);
    console.log(questions);
    console.log(answers);


    response.render('layout', { pageTitle: 'SurveyBuildAtID', template: 'surveyBuildAtID', id: request.params.id, user: request.session.user, survey: survey, questions: questions, answers: answers });
  });

  

  router.post('/', async (request, response) => {
    const { data, user } = request.body;
    console.log('post /surveyBuild: ', data, user);

    const rs = await surveyService.dbInsertSurvey(data, user);

    return response.redirect('/surveyBuild');
    
  });

  return router;
};
