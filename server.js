const express = require('express');
const path = require('path');
const cookieSession = require('cookie-session');
const bodyParser = require('body-parser');


const SpeakersService = require('./services/SpeakerService');
const SurveyService = require('./services/SurveyService');
const LoginService = require('./services/loginService');

const speakersService = new SpeakersService('./data/speakers.json');
const surveyService = new SurveyService('./data/surveys.json');
const loginService = new LoginService('');

const routes = require('./routes');

const app = express();

app.locals.siteName = 'Survey Gen';

const port = 3000;

app.set('trust proxy', 1);

app.use(
  cookieSession({
    name: 'session',
    keys: ['Ghdur687399s7w', 'hhjjdf89s866799'],
  })
);

app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, './views'));

app.locals.siteName = 'Survey Gen';

app.use(express.static(path.join(__dirname, './static')));


app.use(async (request, response, next) => {
  //const surveyLong = await surveyService.getSurveyLong();
  const surveyLongDB = await surveyService.dbgetData();

  //response.locals.surveys = surveyLong;
  response.locals.surveysDB = surveyLongDB;

  return next();
});

app.use(
  '/',
  routes({
    speakersService,
    surveyService,
    loginService,
  })
);

app.listen(port, () => {
  console.log(`Express server listening on port ${port}!`);
});
