// const util = require('util');
const db = require('./db');

/**
 * Logic for reading and writing feedback data
 */
class LoginService {
  /**
   * Constructor
   */
  constructor() {
    this.db = db;
  }

  /**
   * Get all feedback items
   */
  async getList() {
    const data = await this.getData();
    return data;
  }

  /**
   * Fetches feedback data from the JSON file provided to the constructor
   */

  async dbgetList() {
    const data = await this.dbgetData();
    return data;
  }


  async dbInsertUser(email, password) {
    const data = await this.db.dbInsertUser(email, password);
    return data;
  }

  async dbUserIsAdmin(email, password) {
    console.log('loginService dbUserIsAdmin() called: ', email, password);
    const data = await this.db.dbUserIsAdmin(email, password).catch(err => {

      console.log(`loginService dbUserIsAdmin err: ${err}`);
      return [];
    });

    return data;
  }
  
  async dbgetUser(email, password) {
    console.log('loginService dbgetUser() called: ', email, password);
    const data = await this.db.dbgetUser(email, password).catch(err => {
      console.log(`loginService dbgetUser err: ${err}`);
      return [];
    });
    return data;
  }

  async dbgetUserOnEmail(email) {
    console.log('loginService dbgetUserOnEmail() called: ', email);
    const data = await this.db.dbgetUserOnEmail(email).catch(err => {
      console.log(`loginService dbgetUserOnEmail err: ${err}`);
      return [];
    });
    return data;
  }
}

module.exports = LoginService;
