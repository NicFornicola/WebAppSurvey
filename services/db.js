/* eslint-disable no-lonely-if */
const dotenv = require('dotenv');

// const express = require('express');
dotenv.config();

// import pkg from 'pg';
const pkg = require('pg');

const { Pool } = pkg;
let pool = '';

// const dot = require('dotenv').config();
const { DATABASE_URL_LOCAL, DATABASE_URL, DB_PASSWORD } = process.env;
console.log('process.env.NODE_ENV', process.env.NODE_ENV);
 
console.log('DATABASE_URL_LOCAL', DATABASE_URL_LOCAL);
console.log('DATABASE_URL', DATABASE_URL);
console.log('DATABASE_PASSWORD', DB_PASSWORD);

// from docs:
// const db = require('db')
// db.connect({
//   host: process.env.DB_HOST,
//   username: process.env.DB_USER,
//   password: process.env.DB_PASS
// })

if (typeof DATABASE_URL_LOCAL !== 'undefined') {
  console.log('DATABASE_URL_LOCAL', DATABASE_URL_LOCAL);
  pool = new Pool({
    connectionString: DATABASE_URL_LOCAL,
  });
  // console.log('Pool:', pool);
} else {
  if (typeof DATABASE_URL !== 'undefined') {
    console.log('DATABASE_URL', DATABASE_URL);
    pool = new Pool({
      connectionString: DATABASE_URL,
      ssl: { rejectUnauthorized: false },
    });
  } else {
    console.log('ERROR - must define DATABASE_URL in process variables');
  }
}

console.log('index.js starting...\n');

// export async function connectToDatabase() {
// async function connectToDatabase() {
//   return pool;
// }

async function query(sql, errorMessage) {
  try {
    const client = await pool.connect();
    // console.log('after connect client:', client);
    const result = await client.query(sql);
    const results = result ? result.rows : null;
    client.release();

    return results;
    // return {
    //   status: 200,
    //   body: {
    //     results,
    //   },
    // };
  } catch (err) {
    console.error(`ERROR: db ${errorMessage}`, err);

    return {
      status: 500,
      body: {
        error: `ERROR: db ${errorMessage}. ${err}`,
      },
    };
  }
}

async function dbquery(sql, vars = null) {
  try {
    const client = await pool.connect();
    // console.log('after connect client:', client);
    var result = '';
    if (vars == null) {
      result = await client.query(sql);
    } else {
      result = await client.query(sql, vars);
    }
    const results = result ? result.rows : null;
    client.release();

    return results;
    // return {
    //   status: 200,
    //   body: {
    //     results,
    //   },
    // };
  } catch (err) {
    console.error(`ERROR: dbquery ${err}`);

    return {
      status: 500,
      body: {
        error: `ERROR: dbquery ${err}`,
      },
    };
  }
}



async function dbGetAnswers(id) {
  let rs  = await this.dbquery('select s.survey_title, q.survey_id, q.question_id, q.text, a.answer from answers a inner join questions q on (q.question_id = a.question_id) inner join surveys s on (q.survey_id = s.survey_id) where q.survey_id = $1 order by a.answer_id;',
  [id]
  );

  return rs;
}

async function dbGetAllChoices() {
  let rs  = await this.dbquery('select * from choices;',
  );

  return rs;
}

async function dbGetAllUsers() {
  let rs  = await this.dbquery('select * from users;',
  );

  return rs;
}

async function dbGetQuestions(id) {
  let rs  = await this.dbquery('select s.survey_title, s.survey_description, q.survey_id, q.question_id, q.text, q.question_type, q.question_required from questions q inner join surveys s on (q.survey_id = s.survey_id) where q.survey_id = $1 order by q.question_id;',
  [id]
  );

  return rs;
}

async function dbgetSurveys() {
  return this.query('select s.survey_id, u.users_email, s.survey_title, s.survey_description, created_date, edit_date, s.survey_id, a.admin_email from surveys s inner join users u on (s.users_id = u.users_id) inner join admins a on (s.admin_id = a.admin_id);', null);
}

async function dbgetMySurveys(user) {
  let rs = this.dbquery('select s.survey_id, u.users_email, s.survey_title, s.survey_description, created_date, edit_date, s.survey_id, a.admin_email from surveys s inner join users u on (s.users_id = u.users_id) inner join admins a on (s.admin_id = a.admin_id) where u.users_email = $1;',
                       [user]
                       );

  return rs;
}

async function dbgetUserOnEmail(email) {
  console.log('dbgetUserOnEmail email:', email);

  let rs = await this.dbquery(
    'select users_id, users_email from users where users_email = $1;',
    [email]
  ).catch(err => {
    console.log(`db.dbgetUserOnEmail troubles ${err}`);
    return rs;
  });
  console.log('rs: ', rs);
  return rs;
}

async function dbgetUser(email, password) {
  console.log('dbgetUser email:', email, ' password: ', password);
  // sql = 'select id, email, password from users where email = ' + email + ' and password = ' + pwd',
  let rs = await this.dbquery(
    'select users_id, users_email, password from users where users_email = $1 and password = $2;',
    [email, password]
  ).catch(err => {
    console.log(`db.dbgetUser troubles ${err}`);
    return rs;
  });
  console.log('rs: ', rs);
  return rs;
}


async function dbUserIsAdmin(email, password) {
  console.log('dbUserIsAdmin email:', email, ' password: ', password);
  // sql = 'select id, email, password from users where email = ' + email + ' and password = ' + pwd',
  let rs = await this.dbquery(
    'select admins.admin_id, u.users_email from admins inner join users u on (u.users_id = admins.admin_id) where $1 = u.users_email;',
    [email]
  ).catch(err => {
    console.log(`db.dbUserIsAdmin troubles ${err}`);
    return rs;
  });
  console.log('rs: ', rs);
  return rs;
}

async function dbgetSurveysOnId(id) {
  console.log('dbgetSurveysOnId id:', id);
  // sql = 'select id, email, password from users where email = ' + email + ' and password = ' + pwd',
  let rs = await this.dbquery(
    'select * from surveys where $1 = survey_id;',
    [id]
  ).catch(err => {
    console.log(`db.dbgetUser troubles ${err}`);
    return rs;
  });
  console.log('rs: ', rs);
  return rs;
}


async function dbUnApproveSurvey(id) {
  console.log('dbUnApproveSurvey survey:', id);

  let rs = await this.dbquery(
    'select * from surveys where $1 = survey_id;',
    [id]
  ).catch(err => {
    console.log(`db.dbgetUser troubles ${err}`);
    return rs;
  });

  let survey_id          = rs[0].survey_id;
  let survey_title       = rs[0].survey_title;
  let survey_description = rs[0].survey_description;
  let created_date       = rs[0].created_date; 
  let edit_date          = rs[0].edit_date;
  let users_id           = rs[0].users_id; 

  rs = await this.dbquery(
    'delete from surveys where $1 = survey_id;',
    [id]
  ).catch(err => {
    console.log(`db.dbgetUser troubles ${err}`);
    return rs;
  });
 
  rs = await this.dbquery(
    'insert into surveys (survey_id, survey_title, survey_description, created_date, edit_date, users_id, admin_id) values ($1, $2, $3, $4, $5, $6, $7)',
    [survey_id, survey_title, survey_description, created_date, edit_date, users_id, 0]
  ).catch(err => {
    console.log(`dbInsertUser2 this.dbQuery err: ${err}`);
    return [];
  });

  console.log('rs: ', rs);
  return rs;
}

async function dbApproveSurvey(id, user) {
  console.log('dbApproveSurvey user:', user);
  console.log('dbApproveSurvey survey:', id);

  // sql = 'select id, email, password from users where email = ' + email + ' and password = ' + pwd',
  let rs = await this.dbquery(
    'select * from surveys where $1 = survey_id;',
    [id]
  ).catch(err => {
    console.log(`db.dbgetUser troubles ${err}`);
    return rs;
  });

  let survey_id          = rs[0].survey_id;
  let survey_title       = rs[0].survey_title;
  let survey_description = rs[0].survey_description;
  let created_date       = rs[0].created_date; 
  let edit_date          = rs[0].edit_date;
  let users_id           = rs[0].users_id; 

  rs = await this.dbquery(
    'select admin_id from admins where $1 = admin_email;',
    [user]
  ).catch(err => {
    console.log(`db.dbgetUser troubles ${err}`);
    return rs;
  });

  let admin_id = rs[0].admin_id;
  console.log(admin_id);

  rs = await this.dbquery(
    'delete from surveys where $1 = survey_id;',
    [id]
  ).catch(err => {
    console.log(`db.dbgetUser troubles ${err}`);
    return rs;
  });
 
  rs = await this.dbquery(
    'insert into surveys (survey_id, survey_title, survey_description, created_date, edit_date, users_id, admin_id) values ($1, $2, $3, $4, $5, $6, $7)',
    [survey_id, survey_title, survey_description, created_date, edit_date, users_id, admin_id]
  ).catch(err => {
    console.log(`dbInsertUser2 this.dbQuery err: ${err}`);
    return [];
  });

  console.log('rs: ', rs);
  return rs;
}



async function dbGetAllQuestions() {
 

  // sql = 'select id, email, password from users where email = ' + email + ' and password = ' + pwd',
  let rs = await this.dbquery(
    'select * from questions;'
  ).catch(err => {
    console.log(`db.dbgetUser troubles ${err}`);
    return rs;
  });

  return rs;
}




async function  dbInsertChoices(data, q, users_id) {
  console.log('dbInsertChoices called: ', data, q, users_id);

  let rs = await this.dbquery(
    'select choice_id from choices order by choice_id desc limit 1;'
  ).catch(err => {
      console.log(`dbInsertUser1 this.dbQuery err: ${err}`);
      return [];
  });

  let choice_id = rs[0].choice_id + 1;




  for(let i = 0; i < q.length; i++)
  {
    let choice = data[i];
    let question_id = q[i].question_id;
    
    await this.dbquery(
      'insert into choices (choice_id, choice, question_id, users_id) values ($1, $2, $3, $4)',
      [choice_id, choice, question_id, users_id]
    ).catch(err => {
      console.log(`dbInsertUser2 this.dbQuery err: ${err}`);
      return [];
    });
    choice_id++;
  }

  
}

async function  dbDeleteSurvey(id) {

  rs = await this.dbquery(
    'delete from surveys where $1 = survey_id;',
    [id]
  ).catch(err => {
    console.log(`dbInsertUser2 this.dbQuery err: ${err}`);
    return [];
  });
}

async function  dbInsertUser(email, password) {
  console.log('dbInsertUser called: ', email, password);

  let rs = await this.dbquery(
    'select users_id from users order by users_id desc limit 1;'
  ).catch(err => {
      console.log(`dbInsertUser1 this.dbQuery err: ${err}`);
      return [];
  });

  let id = rs[0].users_id + 1;

  rs = await this.dbquery(
    'insert into users (users_id, users_email, password) values ($1, $2, $3)',
    [id, email, password]
  ).catch(err => {
    console.log(`dbInsertUser2 this.dbQuery err: ${err}`);
    return [];
  });
}

async function dbInsertSurvey(data, user) {
  console.log('dbInsertSurvey called: ', data);

  //get biggest id
  let rs = await this.dbquery(
    'select survey_id from surveys order by survey_id desc limit 1;'
  ).catch(err => {
      console.log(`dbInsertUser1 this.dbQuery err: ${err}`);
      return [];
  });

  let survey_id = rs[0].survey_id +1;

  //get user id
  rs = await this.dbquery(
    'select users_id from users where users_email = $1;', [user]
  ).catch(err => {
      console.log(`dbInsertUser1 this.dbQuery err: ${err}`);
      return [];
  });

  let users_id = rs[0].users_id;

  //get user id
  rs = await this.dbquery(
    'select question_id from questions order by question_id desc limit 1;'
  ).catch(err => {
      console.log(`dbInsertUser1 this.dbQuery err: ${err}`);
      return [];
  });

  let question_id = rs[0].question_id + 1;

  rs = await this.dbquery(
    'select answer_id from answers order by answer_id desc limit 1;'
  ).catch(err => {
      console.log(`dbInsertUser1 this.dbQuery err: ${err}`);
      return [];
  });

  let answer_id = rs[0].answer_id + 1;


  let d = JSON.parse(data);
  console.log(d);
  
  let survey_title = d.survey[0].survey_title;
  let survey_description = d.survey[0].survey_description;
  let questions = d.survey[0].questions;

  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + '/' + mm + '/' + dd;
  console.log(survey_title);
  console.log(survey_description);
  console.log(today);
  console.log(users_id);
  console.log(questions);



  rs = await this.dbquery(
    'insert into surveys (survey_id, survey_title, survey_description, created_date, edit_date, users_id, admin_id) values ($1, $2, $3, $4, $5, $6, $7)',
    [survey_id, survey_title, survey_description, today, today, users_id, 0]
  ).catch(err => {
    console.log(`dbInsertUser2 this.dbQuery err: ${err}`);
    return [];
  });


  for(let i = 0; i < questions.length; i++)
  {
    let text = questions[i].questionTitle;
    let question_type = questions[i].questionType;
    let qreq = questions[i].questionRequired;

    let question_required;
    if(qreq == "true")
      question_required = 1;
    else
      question_required = 0;


    rs = await this.dbquery(
      'insert into questions (question_id, text, question_type, question_required, survey_id) values ($1, $2, $3, $4, $5)',
      [question_id, text, question_type, question_required, survey_id]
    ).catch(err => {
      console.log(`dbInsertUser2 this.dbQuery err: ${err}`);
      return [];
    });

    for(let j = 0; j < questions[i].questionAnswers.length; j++)
    {
      if(question_type != "Short Answer")
      {
        answer = questions[i].questionAnswers[j];

        rs = await this.dbquery(
          'insert into answers (answer_id, answer, question_id) values ($1, $2, $3)',
          [answer_id, answer, question_id]
        ).catch(err => {
          console.log(`dbInsertUser2 this.dbQuery err: ${err}`);
          return [];
        });

        answer_id++;
      }
    }

    question_id++;
  }

  
}

module.exports = { dbDeleteSurvey, dbGetAllUsers, dbGetAllChoices, dbGetAllQuestions, dbInsertChoices, dbUnApproveSurvey, dbApproveSurvey, dbgetSurveysOnId, dbGetAnswers,dbGetQuestions, dbgetUserOnEmail, dbgetUser, dbquery, query, dbInsertUser, dbInsertSurvey, dbgetSurveys, dbgetMySurveys, dbUserIsAdmin };
