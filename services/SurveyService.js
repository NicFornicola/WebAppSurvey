const fs = require("fs");
const util = require("util");
const db = require('./db');

/**
 * We want to use async/await with fs.readFile - util.promisfy gives us that
 */
const readFile = util.promisify(fs.readFile);

/**
 * Logic for fetching speakers information
 */
class SurveyService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSOn file that contains the speakers data
   */
  constructor(datafile) {
    this.datafile = datafile;
    this.db = db;
  }

  /**
   * Returns a list of speakers authorName and short authorName
   */
  async getAuthors() {
    const data = await this.getData();

    // We are using map() to transform the array we get into another one
    return data.map(survey => {
      return { 
        authorName: survey.authorName,
        authorEmail: survey.authorEmail };
    });
  }

  /**
   * Get survey information provided a authorEmail
   * @param {*} authorEmail
   */
  async getSurveyByAuthor(authorEmail) {
    const data = await this.getData();
    const survey = data.find(elm => {
      return elm.authorEmail === authorEmail;
    });
    if (!survey) return null;
    return {
      title: survey.title,
      authorName: survey.authorName,
      authorEmail: survey.authorEmail,
      description: survey.description
    };
  }

  /**
   * Returns a list of speakers with only the basic information
   */
  async getSurveyShort() {
    const data = await this.getData();
    return data.map(survey => {
      return {
        authorName: survey.authorName,
        title: survey.title
      };
    });
  }

  /**
   * Get a list of speakers
   */
  async getSurveyReg() {
    const data = await this.getData();
    return data.map(survey => {
      return {
        id: survey.id,
        authorName: survey.authorName,
        title: survey.title,
        desc: survey.desc
      };
    });
  }

  async getSurveyLong() {
    const data = await this.getData();
    return data.map(survey => {
      return {
        id: survey.id,
        authorName: survey.authorName,
        authorEmail: survey.authorEmail,
        shortName: survey.shortName,
        approvedBy: survey.approvedBy,
        dateCreated: survey.dateCreated,
        dateApproved: survey.dateApproved,
        approved: survey.approved,
        title: survey.title,
        desc: survey.desc,
        questions: survey.questions,
      };
    });
  }

  async dbgetData() {
    console.log('dbgetData() called in surveyService.js');
    
    const data = await this.db.dbgetSurveys().catch(err => {
      return [];
    });

    return data;
  }

  async dbgetMySurveys(user) {
    console.log('Getting ' + user + ' surveys called in SurveyService.js');

    const data = await this.db.dbgetMySurveys(user).catch(err => {
      return [];
    });

    return data;
  }

  

  async dbApproveSurvey(id, user) {
    console.log('Approving ' + id  + ' by ' + user + ' called in SurveyService.js');

    const data = await this.db.dbApproveSurvey(id, user).catch(err => {
      return [];
    });

    return data;
  }

  async dbUnApproveSurvey(id) {
    console.log('UnApproving ' + id + ' called in SurveyService.js');

    const data = await this.db.dbUnApproveSurvey(id).catch(err => {
      return [];
    });

    return data;
  }

  async dbgetSurveysOnId(id) {
    console.log('Getting ' + id + ' survey called in SurveyService.js');

    const data = await this.db.dbgetSurveysOnId(id).catch(err => {
      return [];
    });

    return data;
  }

  

  async dbInsertSurvey(data, user) {
    const d = await this.db.dbInsertSurvey(data, user);
    return d;
  }

  async dbInsertChoices(data, q, user_id) {
    const d = await this.db.dbInsertChoices(data, q, user_id);
    return d;
  }

  async dbGetQuestions(id) {
    console.log('Getting survey[' + id + '] called in SurveyService.js');

    const data = await this.db.dbGetQuestions(id).catch(err => {
      return [];
    });

    return data;
  }

  async dbGetAllQuestions() {
    console.log('Getting all questions');

    const data = await this.db.dbGetAllQuestions().catch(err => {
      return [];
    });

    return data;
  }

  async dbGetAllUsers() {
    console.log('Getting all users');

    const data = await this.db.dbGetAllUsers().catch(err => {
      return [];
    });

    return data;
  }
  

  async dbGetAnswers(id) {
    console.log('Getting survey[' + id + '] called in SurveyService.js');

    const data = await this.db.dbGetAnswers(id).catch(err => {
      return [];
    });

    return data;
  }

  async dbGetAllChoices() {
    console.log('Getting all answers');

    const data = await this.db.dbGetAllChoices().catch(err => {
      return [];
    });

    return data;
  }

  async dbDeleteSurvey(id) {

    const data = await this.db.dbDeleteSurvey(id).catch(err => {
      return [];
    });

    return data;
  }


  
  /**
   * Fetches speakers data from the JSON file provided to the constructor
   */
  async getData() {
    const data = await readFile(this.datafile, "utf8");
    return JSON.parse(data).surveys;
  }
}

module.exports = SurveyService;
